import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'InputOutput';
  
  parentValue="I am from parent" // parent value(send to child)
  storeChildData;

  parentFunction(data){
    this.storeChildData = data;
  }
}
