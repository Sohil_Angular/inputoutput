import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-child-component',
  templateUrl: './child-component.component.html',
  styleUrls: ['./child-component.component.css']
})
export class ChildComponentComponent implements OnInit {

  @Input() myValue: string // use for parent to child
  parentValue

  @Output() myOutput: EventEmitter<any> = new EventEmitter(); //use for child to parent
  outPutString = "I am from Child"

  constructor() { }

  ngOnInit(): void {
    this.passValue();
  }

  passValue(){
    this.parentValue = this.myValue
    console.log(this.myValue);
    this.myOutput.emit(this.outPutString);
  }
  searchValue(dateInput) {
    this.myOutput.emit(dateInput);// send data to parent
  }

}
